extends Spatial

export var speed = 600
export var cam_rotation_speed = 0.4

var cam_movement = 0
var cam_strafe = 0
var cam_altitude = 0
var cam_rotate = 0
var cam_direction = Vector3()
var cam_strafe_direction = Vector3()
var cam_altitude_direction = Vector3()

func _ready():
	pass

func _input(event):
	if event is InputEventKey:
		if event.is_action_pressed("move_cam_forward"):
			cam_movement += 1
		if event.is_action_released("move_cam_forward"):
			cam_movement -= 1
		if event.is_action_pressed("move_cam_backwards"):
			cam_movement -= 1
		if event.is_action_released("move_cam_backwards"):
			cam_movement += 1
		
		if event.is_action_pressed("move_cam_up"):
			cam_altitude += 1
		if event.is_action_released("move_cam_up"):
			cam_altitude -= 1
		if event.is_action_pressed("move_cam_down"):
			cam_altitude -= 1
		if event.is_action_released("move_cam_down"):
			cam_altitude += 1
			
		if event.is_action_pressed("move_cam_left"):
			cam_strafe += 1
		if event.is_action_released("move_cam_left"):
			cam_strafe -= 1
		if event.is_action_pressed("move_cam_right"):
			cam_strafe -= 1
		if event.is_action_released("move_cam_right"):
			cam_strafe += 1
		
		if event.is_action_pressed("rotate_cam_left"):
			cam_rotate += 1
		if event.is_action_released("rotate_cam_left"):
			cam_rotate -= 1
		if event.is_action_pressed("rotate_cam_right"):
			cam_rotate -= 1
		if event.is_action_released("rotate_cam_right"):
			cam_rotate += 1
		
var velocity = Vector3()
func _process(delta):
	var target_look_at = get_tree().get_root().get_node("Main").find_node("CamFocusPoint")
	var trans = target_look_at.get_transform()
	# Calculate direction to move camera
	cam_direction = Vector3()
	cam_direction += trans.basis.z
	cam_strafe_direction = Vector3()
	cam_strafe_direction += trans.basis.x
	cam_altitude_direction = Vector3()
	cam_altitude_direction += trans.basis.y
	
	cam_direction = cam_direction.normalized()
	cam_direction = cam_direction * cam_movement * speed * delta
	cam_strafe_direction = cam_strafe_direction.normalized()
	cam_strafe_direction = cam_strafe_direction * cam_strafe * speed * delta
	cam_altitude_direction = cam_altitude_direction.normalized()
	cam_altitude_direction = cam_altitude_direction * cam_altitude * speed * delta
	
	if cam_movement != 0:
		velocity.z = cam_direction.z
	if cam_strafe != 0:
		velocity.x = cam_strafe_direction.x
	if cam_altitude != 0:
		velocity.y = cam_altitude_direction.y
	set_translation(get_translation().linear_interpolate(velocity, delta))
