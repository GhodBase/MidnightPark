extends Spatial

onready var dirt = preload("res://Grounds/DirtGround.tscn")
onready var sStone = preload("res://Decorations/SmallStone.tscn")

var map = [[1, 1, 1,1 ,1 ,1 ,1,1], [2, 2, 2,2 ,2 ,2 ,2,2], [3, 3, 3,3 ,3 ,3,3,3]]

func _ready():
	var surface = [[null, null, sStone, null, null, null,null,null], [null, null, null,null ,null ,null ,null,null], [null, null, null,null ,null ,null,null,null]]

	for xAxis in range(map.size()):
		for zAxis in range(map[xAxis].size()):
			var d = dirt.instance()
			d.set_translation(Vector3(zAxis, 1, xAxis))
			add_child(d)
			if surface[xAxis][zAxis] != null:
				var stone = surface[xAxis][zAxis].instance()
				stone.translation = d.translation
				print(str(d.translation.y))
				stone.translation.y = 0.3
				print(str(stone.translation.y))
				print("Adding stone")
				d.add_child(stone)
